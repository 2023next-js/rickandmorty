
import styles from '@/src/styles/characters.module.css'
const Characters = ({characters,Page}) => {

    return (
                <div className={
                    characters.length>1?styles.gridCard : styles.gridOnly}>
                    {characters.map(e =>(
                    <Page
                        result={e}
                        key={e.id}
                    />
                ))}
                </div>

    )
}

export default Characters
