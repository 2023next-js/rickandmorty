import Link from "next/link";
import Image from 'next/image'

import styles from '@/src/styles/header.module.css'

const Header = () =>{
    return (
        <header className={styles.header}>
            <div className="container">
                <div className={styles.linksNav}>
                    <Link href="/">
                        <Image width={190} height={160} src="/logoRM.ico" alt='Imagen RickMorty'/>
                    </Link>
                    <nav className={styles.navegation}>
                        <Link href="/">Start</Link>
                        <Link href="/about_us">About Us</Link>
                    </nav>
                </div>
            </div>
        </header>
    )
}
export default Header