import Head from 'next/head'
import Header from './Header'


const layout = ({children, title, description}) =>{
    return (
        <>
            <Head>
                <title>-{title}-</title>
                <meta name='description' content={description} />
                <link rel='icon' href='/logoRM.ico'/>//to show icon it must be in public
            </Head>
            <h1> Rick & Morty </h1>
            <Header />
            {children}
        </>
    )
}

export default layout