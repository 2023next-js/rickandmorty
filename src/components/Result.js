import Image from 'next/image'
import styles from "@/src/styles/result.module.css";
import Link from 'next/link'
const Result = ({result}) => {
    const {name, species, id } = result
    return (
        <>
                <Link href={`/only-character?id=${id}`} as={`/only-character?id=${id}`} key={id}>
                    <div className={styles.card} >
                        <div className={styles.cardImg}>
                            <Image width={200} height={200} src={result.image} alt={`image ${name}`} className={styles.cImg}/>
                        </div>
                        <div>
                            <p className={styles.txtTitle}>{name}</p>
                            <p className={styles.txtBody}>Specie: {species}</p>
                            <div className={styles.cardFooter}>
                            <span className="text-title">{result.gender}</span>
                        </div>
                        </div>
                    </div>
                </Link>
        </>
    )
}
export default Result