import styles from "@/src/styles/onlyCharacter.module.css";
import Image from "next/image";
import Link from "next/link"
const CharacterInfo = ({ result }) => {
  //console.log(result.name)Example{id: 3, name: 'Summer Smith', status: 'Alive', species: 'Human', type: '',…}
  const { name, species, gender, image, status } = result;

  return (
    <>
      <div className={styles.cardFirst}>
        <div
          className={status == "Dead" ? styles.divDead : styles.divAlive}
        ></div>
        <div className={styles.divImg}>
          <Image
            width={200}
            height={200}
            src={image}
            alt={`image ${name}`}
            className={styles.dImg}
          />
        </div>
        <div className={styles.divTxt}>
          <div className={styles.divTitleBody}>
          <p className={styles.txtTitle}>{name}</p>
          <p className={styles.txtBody}>Specie: {species}</p>
          <p className={styles.txtBody}> Gender: {gender} </p>
          </div>
          <Link href="/about_us">
            <div className={styles.divLink}>
          <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 448 512">
            <path d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z"/>
          </svg> 
           <p className={styles.txtGoBack}> Back Characters</p>
           </div>
          </Link>
        </div>
      </div>
    </>
  );
};

export default CharacterInfo;
