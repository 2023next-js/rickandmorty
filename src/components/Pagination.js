import styles from '@/src/styles/pagination.module.css'

import {useContext} from "react";
import CharacterContext from "@/src/context/Character/Character-Context";
const CharacterInfo = () => {
    const {getCharacters,currentPage, pageInfo} = useContext(CharacterContext)
   //console.log(result.name)Example{id: 3, name: 'Summer Smith', status: 'Alive', species: 'Human', type: '',…}
    const {pages} = pageInfo
    console.log(currentPage)
    const handlePrev =()=>{

        if(currentPage > 1) getCharacters(currentPage - 1)
    }
const handleNext =()=>{
    if(currentPage < pages){

            getCharacters(currentPage + 1)
    }

}
    return (
        <>
        <div   className={styles.diBttn}>
        <button onClick={handlePrev}  
        className={styles.bttn}>
            <svg  xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 448 512">
                <path d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z"/>
                </svg>
        <p className={styles.pPrePre}>Previous</p>
        </button>
           
        <button onClick={handleNext}  className={styles.bttn}>
            <p className={styles.pPreNex}>Next</p>
        <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 448 512">
            <path d="M438.6 278.6c12.5-12.5 12.5-32.8 0-45.3l-160-160c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L338.8 224 32 224c-17.7 0-32 14.3-32 32s14.3 32 32 32l306.7 0L233.4 393.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l160-160z"/>
            </svg>
        </button> 
        </div>      
        </>
    )
}

export default CharacterInfo