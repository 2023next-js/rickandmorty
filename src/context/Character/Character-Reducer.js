import {GET_CHARACTERS, GET_INFO} from '@/src/context/functions'
/*default function  va devolver un nuevo estado.
state, estado actual.
action, objeto que envia el metodo dispatch.
*/
export default (state, action) =>{
    //desctructuramos action 
    const {payload, type, characters,info, id} = action
    /* Metodos que retornan la actualizacion del estado.
    Esto es útil para restablecer el estado más tarde en respuesta to in action*/
    switch(type){
        case GET_CHARACTERS:
            return {
                ...state,
                characters:payload,
                pageInfo: info,
                currentPage: id
            }
        case GET_INFO:
            console.log('Personajes'+characters.id)
            console.log('personajeee'+payload)
            const resOnlyCharacter = characters.find(item => item.id == payload)
            console.log('onlyyy'+resOnlyCharacter)
            return {
                ...state,
                onlyCharacter:resOnlyCharacter
            }
        default:
            return state
    }
}


