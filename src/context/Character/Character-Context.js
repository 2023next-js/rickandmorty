/*el contexto permite compartir un estado con toda la aplicación 
y el archivo "Character-Context.js" contendrá el contexto y un contenedor para compartir los datos*/
import {createContext} from "react"

const CharacterContext = createContext()

export default CharacterContext