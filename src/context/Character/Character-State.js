import React, {useReducer} from 'react'
import CharacterReducer from '@/src/context/Character/Character-Reducer'
import CharacterContext from '@/src/context/Character/Character-Context'
import axios from 'axios'
const CharacterState = (props) =>{
    //variables de estado incial, 
    const initialState = {
        characters: [],
        onlyCharacter: [],
        currentPage: 1,
        pageInfo: []
    }
    /*state nombre del estado del componente
      Dispatch se encarga de enviar la actualizacion al metodo reducer
    CharacterReducer va retornar el nuevo estado
    */
  const [state, dispatch] = useReducer(CharacterReducer, initialState)
    const getCharacters = async (id=1) => {
        try {
            //console.log("getCharacter,Entre")
            const res = await axios.get('https://rickandmortyapi.com/api/character?page='+ id)
            const data = res.data.results
            //info, para ver la cantidad y demas info de paginas
            const info = res.data.info
            dispatch({
                type: 'GET_CHARACTERS',
                payload: data, info, id
            })
        } catch (error) {
            console.error(error)
        }
    }
    const getInfo = async(id) => {
        try {
            
            const characters = state.characters
            dispatch({
                type: 'GET_INFO',
                payload: id, characters
            })
        }catch (error){ }
    }
    //Las variables de estadoinicial se usará para almacenar el estado inicial de la aplicación
    return(
        <CharacterContext.Provider value={{
         characters: state.characters,
         onlyCharacter: state.onlyCharacter,
         currentPage: state.currentPage,
         pageInfo: state.pageInfo,
         getCharacters,
         getInfo
        }}>
            {props.children}
        </CharacterContext.Provider>
    )
}
export default CharacterState