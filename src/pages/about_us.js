import Layout from "@/src/components/Layout";
import Characters from "@/src/components/characters";
import Result from "@/src/components/Result";
import { useContext, useEffect,useMemo } from "react";
import CharacterContext from "@/src/context/Character/Character-Context";
import Pagination from "@/src/components/Pagination";

const AboutUs = () => {
  const { characters, getCharacters, currentPage } =
    useContext(CharacterContext);

  useEffect(() => {
    getCharacters(currentPage);
  }, []);

  return (
    <Layout title="Characters" description="Rick and Morty Character">
      <main className="container">
        <h2>Characters</h2>
        <Pagination />
        <Characters characters={characters} Page={Result} />
        <Pagination />
      </main>
    </Layout>
  );
};
export default AboutUs;
