import Layout from '@/src/components/Layout'
import Characters from "@/src/components/characters"
import CharacterInfo from "@/src/components/Character-Info"
import {useContext, useEffect} from "react";
import CharacterContext from "@/src/context/Character/Character-Context";
import {useRouter} from "next/router";
const AboutUs = () => {
    const {onlyCharacter, getInfo} = useContext(CharacterContext)
    const router = useRouter()
    const {id} = router.query
    useEffect(() => {
        getInfo(id)
    }, [])
    return (
        <Layout title='Characters' description='Rick and Morty Character'>
            <h2>Profile Me</h2>
            <Characters characters={[onlyCharacter]} Page={CharacterInfo} />
        </Layout>
    )}
export default AboutUs
