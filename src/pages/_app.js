//importamos nuestro "Character-State.js" y emvolveremos nuestro <Component {...pageProps} />
import '@/src/styles/globals.css'
import CharacterState from "@/src/context/Character/Character-State";

function MyApp ({Component, pageProps}){
    return(
        <CharacterState>
        <Component {...pageProps} />
        </CharacterState>
    )
}

export default MyApp